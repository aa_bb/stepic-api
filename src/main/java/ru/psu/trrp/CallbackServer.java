package ru.psu.trrp;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.CompletableFuture;

class CallbackServer {
    private final HttpServer server;
    private final CompletableFuture<String> oAuth2VerifierCF = new CompletableFuture<>();

    CallbackServer() throws IOException {
        server = HttpServer.create(new InetSocketAddress(8080), 0);
        server.createContext("/", new OAuth2CallbackHandler(this));
        server.setExecutor(null);
    }

    void start() {
        server.start();
    }

    CompletableFuture<String> getOAuth2VerifierCF() {
        return oAuth2VerifierCF;
    }

    void setOAuth2Verifier(String value) {
        oAuth2VerifierCF.complete(value);
        server.stop(0);
    }

}
