package ru.psu.trrp;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.oauth.OAuth20Service;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

class RequestExecutor {
    private OAuth2AccessToken accessToken;
    private final OAuth20Service service;
    private final FileStorage<OAuth2AccessToken> fs = new FileStorage<>();
    private static CallbackServer cbs;

    RequestExecutor() throws IOException, ExecutionException, InterruptedException, URISyntaxException {
        String clientId = "NrAHVRdWkGGyzlsYpwVAhiVBJxNoJ8po9ZHLasse";
        String clientSecret = "wscCCnSoE01sxgfHc2SNyPEZOF6iE9qMZRdub6W29WlIzSAs8Piyr4liPGJSioAm3IqhdEpalg54CeIQZTTqzJDypX0sCsEwCahRvbRof8owhmmhEr9xeNCtMUnVgVie";
        service = new ServiceBuilder(clientId)
                .apiSecret(clientSecret)
                .callback("http://localhost:8080/")
                .build(StepicApi.instance());
        accessToken = fs.loadFromFile(OAuth2AccessToken.class);
        if (accessToken == null) {
            cbs = new CallbackServer();
            cbs.start();
            authenticate(cbs);
        }
    }

    private void authenticate(CallbackServer cbs) throws InterruptedException, ExecutionException, IOException, URISyntaxException {
        String authUrl = service.getAuthorizationUrl();
        Desktop.getDesktop().browse(new URI(authUrl));
        String code = cbs.getOAuth2VerifierCF().get();
        accessToken = service.getAccessToken(code);
        fs.saveToFile(accessToken);
    }

    public Response execute(OAuthRequest request) throws InterruptedException, ExecutionException, URISyntaxException, IOException {
        if (accessToken == null) {
            cbs = new CallbackServer();
            cbs.start();
            authenticate(cbs);
        }

        service.signRequest(accessToken, request);
        Response response = null;
        try {
            response = service.execute(request);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return response;
    }

    public void logout() throws IOException {
        accessToken = null;
        fs.cleanFile();
    }

    public boolean isAushentificated() {
        return accessToken != null;
    }
}
