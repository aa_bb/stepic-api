package ru.psu.trrp.Model;

public class Lesson {
    private long id;
    private long[] steps;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long[] getSteps() {
        return steps;
    }

    public void setSteps(long[] steps) {
        this.steps = steps;
    }
}
