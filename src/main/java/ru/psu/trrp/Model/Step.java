package ru.psu.trrp.Model;

public class Step {
    private long id;
    private long lesson;
    private Block block;
    //private int position;

    public Step() {}

    public Step(long lesson, Block block) {
        this.lesson = lesson;
        this.block = block;
        //this.position = position;
    }

    public Step(long id, long lesson, Block block) {
        this.id = id;
        this.lesson = lesson;
        this.block = block;
        //this.position = position;
    }

//    public int getPosition() {
//        return position;
//    }
//
//    public void setPosition(int position) {
//        this.position = position;
//    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLesson() {
        return lesson;
    }

    public void setLesson(long lesson) {
        this.lesson = lesson;
    }
}
