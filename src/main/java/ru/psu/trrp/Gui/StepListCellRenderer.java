package ru.psu.trrp.Gui;

import ru.psu.trrp.Model.Step;

import javax.swing.*;
import java.awt.*;

public class StepListCellRenderer extends DefaultListCellRenderer {
    public Component getListCellRendererComponent(JList<?> list, Object value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (value instanceof Step) {
            Step step = (Step)value;
            String text = step.getBlock().getText();
            if (text.length() >= 25)
                setText(text.substring(0, 25) + "...");
            else
                setText(text);
            setToolTipText(String.valueOf(step.getId()));
        }
        return this;
    }
}
