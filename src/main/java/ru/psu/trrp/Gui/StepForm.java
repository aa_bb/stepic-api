package ru.psu.trrp.Gui;

import ru.psu.trrp.Model.Block;
import ru.psu.trrp.Model.Lesson;
import ru.psu.trrp.Model.Step;
import ru.psu.trrp.Stepic;

import javax.swing.*;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.awt.*;
import java.util.concurrent.ExecutionException;

public class StepForm extends JFrame {
    private JPanel panel;
    private JLabel lblLesson;
    private JLabel contentTextLbl;
    private JTextArea txtAreaContent;
    private JButton btnOk;
    private JComboBox<String> lessonComboBox;

    private Mode mode;
    private Step step;
    private List<Lesson> lessons;

    public StepForm(Mode mode, Step step, List<Lesson> lessons, MainForm parent) {
        this.mode = mode;
        this.step = step;
        this.lessons = lessons;

        setContentPane(panel);
        panel.setPreferredSize(new Dimension(400, 230));
        setResizable(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        txtAreaContent.setLineWrap(true);

        switch(mode) {
            case View:
                setTitle("Просмотр степа");
                break;
            case Edit:
                setTitle("Редактирование степа");
                break;
            case Create:
                setTitle("Создание нового степа");
                break;
        }

        int cbxSelect = -1;
        for (int i = 0; i < lessons.size(); i++) {
            lessonComboBox.addItem(lessons.get(i).getTitle());
            if (lessons.get(i).getId() == step.getLesson())
                cbxSelect = i;
        }

        lessonComboBox.setSelectedIndex(cbxSelect);
        if (mode != Mode.Create) {
            txtAreaContent.setText(step.getBlock().getText());
        }

        if (mode == Mode.View) {
            txtAreaContent.setEditable(false);
            setJComboBoxReadOnly(lessonComboBox);
        }

        btnOk.addActionListener(e -> {
            if (mode != Mode.View) {
                step.setBlock(new Block("text", txtAreaContent.getText()));
                step.setLesson(lessons.get(lessonComboBox.getSelectedIndex()).getId());
            }
            try {
                parent.callStepicServer(mode, step);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            parent.unblockAllBtns();
            dispose();
        });

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        pack();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                parent.unblockAllBtns();
            }
        });
        parent.blockAllBtns();
        setVisible(true);
    }

    private void setJComboBoxReadOnly(JComboBox<String> jcb)
    {
        JTextField jtf = (JTextField)jcb.getEditor().getEditorComponent();
        jtf.setEditable(false);

        MouseListener[] mls = jcb.getMouseListeners();
        for (MouseListener listener : mls)
            jcb.removeMouseListener(listener);

        Component[] comps = jcb.getComponents();
        for (Component c : comps)
        {
            if (c instanceof AbstractButton)
            {
                AbstractButton ab = (AbstractButton)c;
                ab.setEnabled(false);

                MouseListener[] mls2 = ab.getMouseListeners();
                for (MouseListener listener : mls2)
                    ab.removeMouseListener(listener);
            }
        }
    }
}
