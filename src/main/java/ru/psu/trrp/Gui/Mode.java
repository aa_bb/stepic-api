package ru.psu.trrp.Gui;

public enum Mode {
    View,
    Edit,
    Create,
    Delete
}
