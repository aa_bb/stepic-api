package ru.psu.trrp.Gui;

import ru.psu.trrp.Model.Lesson;
import ru.psu.trrp.Model.Step;
import ru.psu.trrp.Stepic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainForm extends JFrame {
    private JList<Lesson> lessonList;
    private JList<Step> stepList;
    private JButton addStepButton;
    private JButton viewStepButton;
    private JButton editStepButton;
    private JButton deleteStepButton;
    private JPanel panel1;
    private JLabel logoutLink;
    private final MainForm me = this;

    private final Stepic stepic;
    private DefaultListModel<Lesson> lessonListModel;
    private List<Lesson> lessons;
    private DefaultListModel<Step>[] stepListModels;
    private int selectedStepIndex = -1;
    private int selectedLessonIndex = -1;

    public MainForm() throws InterruptedException, ExecutionException, IOException, URISyntaxException {
        defineGUI();
        stepic = new Stepic();
        prepareLists();

        setButtons();
        logoutLink.addMouseListener(setLogoutMouseListener());
        setVisible(true);
    }

    public void callStepicServer(Mode mode, Step step) throws InterruptedException, ExecutionException, URISyntaxException, IOException {
        switch(mode) {
            case Edit:
                stepic.editStep(step);
                stepListModels[selectedLessonIndex].setElementAt(step, selectedStepIndex);
                break;
            case Create:
                long id = stepic.createStep(step);
                step.setId(id);
                stepListModels[selectedLessonIndex].addElement(step);
                break;
            case Delete:
                stepic.deleteStep(step.getId());
                stepListModels[selectedLessonIndex].removeElementAt(selectedStepIndex);
                break;
        }
        if (stepic.isAuthentificated())
            logoutLink.setText("Выйти");
    }

    private void defineGUI() {
        setContentPane(panel1);
        panel1.setPreferredSize(new Dimension(640, 480));
        setResizable(false);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }

    private void fillLessonLibrary() throws InterruptedException, ExecutionException, URISyntaxException, IOException {
        lessons = stepic.getLessons();
        if (stepic.isAuthentificated())
            logoutLink.setText("Выйти");
        for (Lesson lesson : lessons) {
            lessonListModel.addElement(lesson);
        }
    }

    private void fillStepLibrary() throws InterruptedException, ExecutionException, URISyntaxException, IOException {
        for (int i = 0; i < lessonListModel.size(); i++) {
            stepListModels[i] = new DefaultListModel<>();
            for (long stepId : lessonListModel.getElementAt(i).getSteps()) {
                Step step = stepic.getStepById(stepId);
                if (stepic.isAuthentificated())
                    logoutLink.setText("Выйти");
                stepListModels[i].addElement(step);
            }
        }
    }

    private void prepareLists() throws InterruptedException, ExecutionException, IOException, URISyntaxException {
        lessonList.setCellRenderer(new LessonListCellRenderer());
        stepList.setCellRenderer(new StepListCellRenderer());

        lessonListModel = new DefaultListModel<>();
        fillLessonLibrary();
        stepListModels = new DefaultListModel[lessonListModel.size()];
        fillStepLibrary();

        lessonList.setModel(lessonListModel);
        lessonList.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                int n = lessonList.getSelectedIndex();
                stepList.setModel(stepListModels[n]);
                selectedLessonIndex = n;
            }
        });

        stepList.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                int n = stepList.getSelectedIndex();
                selectedStepIndex = n;
            }
        });
    }

    public void blockAllBtns() {
        addStepButton.setEnabled(false);
        editStepButton.setEnabled(false);
        viewStepButton.setEnabled(false);
        deleteStepButton.setEnabled(false);
    }

    public void unblockAllBtns() {
        addStepButton.setEnabled(true);
        editStepButton.setEnabled(true);
        viewStepButton.setEnabled(true);
        deleteStepButton.setEnabled(true);
    }

    private void setButtons() {
        addStepButton.addActionListener(e -> {
            if (selectedLessonIndex == -1) {
                JOptionPane.showMessageDialog(null, "Сначала выберите урок!");
                return;
            }
            Step step = new Step();
            step.setLesson(lessonListModel.get(selectedLessonIndex).getId());
            new StepForm(Mode.Create, step, lessons, me);
        });
        editStepButton.addActionListener(e -> {
            if (selectedStepIndex == -1) {
                JOptionPane.showMessageDialog(null, "Сначала выберите степ!");
                return;
            }
            Step step = stepListModels[selectedLessonIndex].elementAt(selectedStepIndex);
            new StepForm(Mode.Edit, step, lessons, me);
        });
        viewStepButton.addActionListener(e -> {
            if (selectedStepIndex == -1) {
                JOptionPane.showMessageDialog(null, "Сначала выберите степ!");
                return;
            }
            Step step = stepListModels[selectedLessonIndex].elementAt(selectedStepIndex);
            new StepForm(Mode.View, step, lessons, me);
        });
        deleteStepButton.addActionListener(e -> {
            if (selectedStepIndex == -1) {
                JOptionPane.showMessageDialog(null, "Сначала выберите степ!");
                return;
            }
            Step step = stepListModels[selectedLessonIndex].elementAt(selectedStepIndex);
            try {
                callStepicServer(Mode.Delete, step);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    private MouseListener setLogoutMouseListener() {
        return new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    stepic.logout();
                    JOptionPane.showMessageDialog(null, "Отлично, вы успешно разлогинились");
                    logoutLink.setText("");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }

            @Override public void mousePressed(MouseEvent e) { }
            @Override public void mouseReleased(MouseEvent e) { }
            @Override public void mouseEntered(MouseEvent e) { }
            @Override public void mouseExited(MouseEvent e) { }
        };
    }
}
