package ru.psu.trrp.Gui;

import ru.psu.trrp.Model.Lesson;

import javax.swing.*;
import java.awt.*;

public class LessonListCellRenderer extends DefaultListCellRenderer {
    public Component getListCellRendererComponent(JList<?> list, Object value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (value instanceof Lesson) {
            Lesson lesson = (Lesson)value;
            String text = lesson.getTitle();
            if (text.length() >= 25)
                setText(text.substring(0, 25) + "...");
            else
                setText(text);
            setToolTipText(String.valueOf(lesson.getId()));
        }
        return this;
    }
}
