package ru.psu.trrp;

import com.github.scribejava.core.builder.api.DefaultApi20;

public class StepicApi extends DefaultApi20 {

    public StepicApi() {
    }

    private static class InstanceHolder {
        private static final StepicApi INSTANCE = new StepicApi();
    }

    public static StepicApi instance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return "https://stepik.org/oauth2/token/";
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return "https://stepik.org/oauth2/authorize/";
    }
}
