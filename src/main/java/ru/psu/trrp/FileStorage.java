package ru.psu.trrp;

import com.google.gson.Gson;

import javax.crypto.Cipher;
import java.io.*;
import java.util.Scanner;

public class FileStorage<T> {
    private static final String filename = "accessCode.txt";
    static Gson gson = new Gson();

    public T loadFromFile(Class<T> clazz) throws FileNotFoundException {
        T result = null;
        StringBuilder message = new StringBuilder();
        File myObj = new File(filename);
        Scanner myReader = new Scanner(myObj);
        while (myReader.hasNextLine())
            message.append(myReader.nextLine());
        if (message.length() != 0) {
            String decrypted = Aes256Cipher.decrypt(message.toString());
            result = gson.fromJson(decrypted, clazz);
        }
        myReader.close();
        return result;
    }

    public void saveToFile(T input) throws IOException {
        FileWriter myWriter = new FileWriter(filename);
        String message = gson.toJson(input);
        String encrypted = Aes256Cipher.encrypt(message);
        myWriter.write(encrypted);
        myWriter.close();
    }

    public void cleanFile() throws IOException {
        FileWriter writer = new FileWriter(filename);
        writer.write("");
        writer.close();
    }
}
