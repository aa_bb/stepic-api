package ru.psu.trrp;

import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import ru.psu.trrp.Model.Lesson;
import ru.psu.trrp.Model.Step;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Stepic {
    public static final String urlPrefix = "https://stepic.org/api";
    private final RequestExecutor requestExecutor;

    public Stepic() throws InterruptedException, ExecutionException, URISyntaxException, IOException {
        requestExecutor = new RequestExecutor();
    }

    public List<Lesson> getLessons() throws IOException, InterruptedException, ExecutionException, URISyntaxException {
        System.out.println(">> getLessons");
        OAuthRequest oAuthRequest = new OAuthRequest(Verb.GET, urlPrefix + "/lessons/?course=84315");
        Response response = requestExecutor.execute(oAuthRequest);

        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonArray lessonsJsonArray = parser.parse(response.getBody())
                .getAsJsonObject()
                .getAsJsonArray("lessons");
        return Arrays.asList(gson.fromJson(lessonsJsonArray, Lesson[].class));
    }

    public Step getStepById(long id) throws IOException, InterruptedException, ExecutionException, URISyntaxException {
        System.out.println(">> getStepByID_" + id);
        OAuthRequest oAuthRequest = new OAuthRequest(Verb.GET, urlPrefix + "/step-sources/" + id);
        Response response = requestExecutor.execute(oAuthRequest);

        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonArray stepSourcesJsonArray = parser.parse(response.getBody())
                .getAsJsonObject()
                .getAsJsonArray("step-sources");

        return Arrays.asList(gson.fromJson(stepSourcesJsonArray, Step[].class)).get(0);
    }

    public long createStep(Step step) throws IOException, InterruptedException, ExecutionException, URISyntaxException {
        System.out.println(">> createStep");
        OAuthRequest oAuthRequest = new OAuthRequest(Verb.POST, urlPrefix + "/step-sources");

        Gson gson = new Gson();
        String json = gson.toJson(step);
        json = "{\"stepSource\": " + json + "}";

        oAuthRequest.addHeader("Content-Type", "application/json;charset=UTF-8");
        oAuthRequest.setPayload(json);

        Response response = requestExecutor.execute(oAuthRequest);

        JsonParser parser = new JsonParser();
        JsonArray lessonJsonObject = parser.parse(response.getBody())
                .getAsJsonObject()
                .getAsJsonArray("step-sources");
        return Arrays.asList(gson.fromJson(lessonJsonObject, Step[].class)).get(0).getId();
    }

    public void editStep(Step step) throws IOException, InterruptedException, ExecutionException, URISyntaxException {
        System.out.println(">> editStep");
        OAuthRequest oAuthRequest = new OAuthRequest(Verb.PUT, urlPrefix + "/step-sources/" + step.getId());

        Gson gson = new Gson();
        String json = gson.toJson(step);
        json = "{\"stepSource\": " + json + "}";

        oAuthRequest.addHeader("Content-Type", "application/json;charset=UTF-8");
        oAuthRequest.setPayload(json);

        requestExecutor.execute(oAuthRequest);
    }

    public void deleteStep(long id) throws IOException, InterruptedException, ExecutionException, URISyntaxException {
        System.out.println(">> deleteStepByID_" + id);
        OAuthRequest oAuthRequest = new OAuthRequest(Verb.DELETE, urlPrefix + "/step-sources/" + id);
        requestExecutor.execute(oAuthRequest);
    }

    public void logout() throws IOException {
        requestExecutor.logout();
    }

    public boolean isAuthentificated() {
        return requestExecutor.isAushentificated();
    }
}

